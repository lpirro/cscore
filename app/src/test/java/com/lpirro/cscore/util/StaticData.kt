package com.lpirro.cscore.util

import com.lpirro.cscore.network.models.CreditReport
import com.lpirro.cscore.network.models.CreditReportInfo

object StaticData {
    fun generateCreditReport(): CreditReport {
        return CreditReport(generateCreditReportInfo())
    }

    private fun generateCreditReportInfo(): CreditReportInfo {
        return CreditReportInfo(50, 700, 0)
    }
}
