package com.lpirro.cscore.app.scorereport

import com.lpirro.cscore.network.api.ApiService
import com.lpirro.cscore.network.api.Repository
import com.lpirro.cscore.network.models.CreditReport
import com.lpirro.cscore.util.StaticData
import com.lpirro.cscore.utils.TestSchedulerProvider
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class ScoreReportFragmentTest {

    private lateinit var presenter: ScoreReportPresenter
    private var view = mock<ScoreReportContract.ScoreReportView>()
    private val service = mock<ApiService>()
    private var repo = mock<Repository>()

    @Before
    fun setup() {
        repo.apiService = service
        presenter = ScoreReportPresenter(repo, TestSchedulerProvider())
        presenter.attachView(view)
    }

    @Test
    fun `get credit report should show credit report`() {
        val creditReport = StaticData.generateCreditReport()
        val single: Single<CreditReport> = Single.just(creditReport)

        doReturn(single).`when`(repo.apiService).getCreditReport()

        presenter.loadScoreReport()

        verify(view).showScoreReport(creditReport.creditReportInfo)
    }

    @Test
    fun `get credit report error should show error message`() {

        doReturn(Single.error<Any>(Exception("dummy exception"))).`when`(repo.apiService).getCreditReport()

        presenter.loadScoreReport()
        verify(view).showError(any())
    }

    @Test
    fun `get credit report should show loading`() {
        val creditReport = StaticData.generateCreditReport()
        val single: Single<CreditReport> = Single.just(creditReport)

        doReturn(single).`when`(repo.apiService).getCreditReport()


        val inOrder = Mockito.inOrder(view)
        presenter.loadScoreReport()

        inOrder.verify(view).showLoading(true)
        inOrder.verify(view).showLoading(false)

    }




}