package com.lpirro.cscore.mvp

interface View {
    fun showLoading(active: Boolean)

    fun showError(error: Throwable)
}