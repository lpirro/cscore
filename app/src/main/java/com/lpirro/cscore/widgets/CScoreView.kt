package com.lpirro.cscore.widgets

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.graphics.Paint.Align
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.content.ContextCompat
import com.lpirro.cscore.R
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

/**
 * Clear Score View
 *
 * A view that shows a donut graph with a percentage progress
 *
 * @property maxValue defines the max value that you can set
 * @property minValue defines the min value that you can set
 * @property startAngle defines the starting angle of percentage circle
 *
 */

class CScoreView : View {

    private var drawingArea: RectF? = null
    private lateinit var outerCirclePaint: Paint
    private lateinit var progressCirclePaint: Paint
    private lateinit var textPaint: Paint
    private lateinit var scorePaint: Paint

    private var centerX: Int = 0
    private var centerY: Int = 0
    private var radius: Int = 0

    var minValue = 0f
    var maxValue = 0f
    var startAngle = -90f

    private var sweepAngle = 0f
    private var currentValue = 0f
    private var outerCircleStrokeSize = 10f
    private var progressCircleStrokeSize = 15f
    private var outerCircleColor = 0
    private var progressCircleColor = 0
    private var progressCircleSpacing = 0f
    private var textSpacing = 0f

    private var textSize = 40f
    private var scoreSize = 120f

    private lateinit var gradient: SweepGradient

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        readAttributes(attrs)

        textPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        textPaint.color = Color.BLACK
        textPaint.textSize = textSize
        textPaint.textAlign = Align.CENTER

        scorePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        scorePaint.color = Color.BLUE
        scorePaint.textSize = scoreSize
        scorePaint.textAlign = Align.CENTER

        outerCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        outerCirclePaint.style = Paint.Style.STROKE
        outerCirclePaint.strokeWidth = outerCircleStrokeSize
        outerCirclePaint.color = outerCircleColor

        progressCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        progressCirclePaint.style = Paint.Style.STROKE
        progressCirclePaint.strokeWidth = progressCircleStrokeSize

    }

    /**
     * Draw all the elements on the screen
     */

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (drawingArea == null) {
            // take the minimum of width and height here to be on he safe side:
            centerX = measuredWidth / 2
            centerY = measuredHeight / 2
            radius = Math.min(centerX, centerY)

            val startTopOuter = outerCircleStrokeSize / 2
            val endBottomOuter = 2 * radius - startTopOuter

            drawingArea = RectF(
                startTopOuter.toInt() + progressCircleSpacing,
                startTopOuter.toInt() + progressCircleSpacing,
                endBottomOuter.toInt() - progressCircleSpacing,
                endBottomOuter.toInt() - progressCircleSpacing
            )
            gradient = SweepGradient(centerX.toFloat(), 0f, Color.YELLOW, Color.RED)
        }

        progressCirclePaint.shader = gradient
        scorePaint.shader = gradient
        canvas.drawCircle(centerX.toFloat(), centerY.toFloat(), radius - outerCircleStrokeSize / 2, outerCirclePaint)
        canvas.drawArc(drawingArea, startAngle, sweepAngle, false, progressCirclePaint)

        val headerText = resources.getString(R.string.credit_score_title)
        val scoreText = reverseSweepAngle(sweepAngle).roundToInt().toString()
        val bottomText = resources.getString(R.string.credit_score_out_score, maxValue.roundToInt())

        val xPosScore = width / 2.toFloat()
        val yPosScore = height / 2 - (scorePaint.descent() + scorePaint.ascent()) / 2
        val xPosText = width / 2.toFloat()
        val yPosText = height / 2 - (textPaint.descent() + textPaint.ascent()) / 2

        canvas.drawText(headerText, xPosText, yPosText + scorePaint.fontMetrics.ascent, textPaint)
        canvas.drawText(scoreText, xPosScore, yPosScore, scorePaint)
        canvas.drawText(bottomText, xPosText, yPosText - scorePaint.fontMetrics.ascent, textPaint)

    }

    /**
     * Set the current value on the Clear Score View
     *
     * @property value the value you want to set
     * @property animation if you want to set the value with an animation
     *
     */

    fun setValue(value: Float, animation: Boolean = true) {
        checkValidArgument(value)

        this.sweepAngle = calculateSweepAngle(value)

        if (animation) playAnimation()
    }

    /**
     * Normalize the value to fit in a 360° circle
     */

    private fun calculateSweepAngle(value: Float): Float {
        val chartValuesWindow = Math.max(minValue, maxValue) - Math.min(minValue, maxValue)
        val chartValuesScale = 360f / chartValuesWindow
        return value * chartValuesScale
    }

    /**
     * De-normalize the value
     */

    private fun reverseSweepAngle(value: Float): Float {
        val chartValuesWindow = Math.max(minValue, maxValue) - Math.min(minValue, maxValue)
        val chartValuesScale = 360f / chartValuesWindow
        return value / chartValuesScale
    }

    /**
     * This method will perform an animation when drawing the percentage
     */

    private fun playAnimation() {
        val valueAnimation = ValueAnimator.ofFloat(currentValue, sweepAngle)
        valueAnimation.duration = TimeUnit.MILLISECONDS.toMillis(900)
        valueAnimation.interpolator = AccelerateDecelerateInterpolator()
        valueAnimation.addUpdateListener() {
            drawProgress(it.animatedValue as Float)
        }
        valueAnimation.start()
    }


    /**
     * Draw the content on the view
     * @param progress the progress of the animatioj
     */

    private fun drawProgress(progress: Float) {
        sweepAngle = progress
        currentValue = progress
        invalidate()
    }

    /**
     * This methods reads the attributes written in xml
     */

    private fun readAttributes(attrs: AttributeSet?) {
        val resources = context.resources

        outerCircleColor = ContextCompat.getColor(context, R.color.default_outer_circle_color)
        outerCircleStrokeSize = resources.getDimension(R.dimen.default_stroke_size_outer_circle)
        progressCircleStrokeSize = resources.getDimension(R.dimen.default_stroke_size_progress_circle)
        progressCircleSpacing = resources.getDimension(R.dimen.default_progress_circle_spacing)

        attrs?.let {
            val attributes = context
                .theme.obtainStyledAttributes(attrs, R.styleable.ScoreView, 0, 0)

            textSize =
                    attributes.getDimensionPixelSize(
                        R.styleable.ScoreView_descriptionTextSize,
                        textSize.toInt()
                    ).toFloat()

            scoreSize =
                    attributes.getDimensionPixelSize(
                        R.styleable.ScoreView_scoreTextSize,
                        scoreSize.toInt()
                    ).toFloat()

            outerCircleStrokeSize =
                    attributes.getDimensionPixelSize(
                        R.styleable.ScoreView_outerCircleStrokeSize,
                        outerCircleStrokeSize.toInt()
                    ).toFloat()

            progressCircleStrokeSize =
                    attributes.getDimensionPixelSize(
                        R.styleable.ScoreView_progressCircleStrokeSize,
                        outerCircleStrokeSize.toInt()
                    ).toFloat()

            outerCircleColor =
                    attributes.getColor(
                        R.styleable.ScoreView_outerCircleColor,
                        outerCircleColor
                    )

            progressCircleColor =
                    attributes.getColor(
                        R.styleable.ScoreView_progressCircleColor,
                        progressCircleColor
                    )

            progressCircleSpacing =
                    attributes.getDimensionPixelSize(
                        R.styleable.ScoreView_progressCircleSpacing,
                        progressCircleSpacing.toInt()
                    ).toFloat()

            maxValue = attributes.getFloat(R.styleable.ScoreView_maxValue, 0f)
            minValue = attributes.getFloat(R.styleable.ScoreView_minValue, 0f)
        }

        textSpacing = progressCircleSpacing + 30f
    }


    /**
     * Save the instance state of the view to persist on Orientation Changes, etc
     */

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val state = CScoreViewState(superState)

        state.maxValue = this.maxValue
        state.minValue = this.minValue
        state.currentValue = this.currentValue
        state.sweepAngle = this.sweepAngle

        return state
    }

    /**
     * Restore the instance state of the view
     */

    override fun onRestoreInstanceState(state: Parcelable?) {

        if (state !is CScoreViewState) {
            super.onRestoreInstanceState(state)
            return
        }

        this.maxValue = state.maxValue
        this.minValue = state.minValue
        this.currentValue = state.currentValue
        this.sweepAngle = state.sweepAngle

        super.onRestoreInstanceState(state.superState)
    }


    /**
     * Checks if the value passed by setValue() is contained between minValue and maxValue, if not it throws an Exception
     * @throws IllegalArgumentException
     */

    private fun checkValidArgument(value: Float) {
        val range = minValue..maxValue

        when {
            maxValue == 0f -> throw IllegalArgumentException("maxValue not set, you can set it by calling the CScoreView.maxValue(value) method")
            value !in range -> throw IllegalArgumentException("The value you are passing ($value) should be between $minValue (minValue) and $maxValue (maxValue)")
        }
    }
}
