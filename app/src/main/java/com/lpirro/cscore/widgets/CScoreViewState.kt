package com.lpirro.cscore.widgets

import android.os.Parcel
import android.os.Parcelable
import android.preference.Preference

internal class CScoreViewState : Preference.BaseSavedState {
    var maxValue: Float = 0f
    var minValue: Float = 0f
    var currentValue: Float = 0f
    var sweepAngle: Float = 0f

    constructor(superState: Parcelable) : super(superState)

    private constructor(parcel: Parcel) : super(parcel) {
        this.maxValue = parcel.readFloat()
        this.minValue = parcel.readFloat()
        this.currentValue = parcel.readFloat()
        this.sweepAngle = parcel.readFloat()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        parcel.writeFloat(this.maxValue)
        parcel.writeFloat(this.minValue)
        parcel.writeFloat(this.currentValue)
        parcel.writeFloat(this.sweepAngle)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<CScoreViewState> = object : Parcelable.Creator<CScoreViewState> {
            override fun createFromParcel(parcel: Parcel): CScoreViewState {
                return CScoreViewState(parcel)
            }

            override fun newArray(size: Int): Array<CScoreViewState?> {
                return arrayOfNulls(size)
            }
        }
    }
}