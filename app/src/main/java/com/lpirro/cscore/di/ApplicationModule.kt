package com.lpirro.cscore.di

import android.app.Application
import android.content.Context
import com.lpirro.cscore.network.api.Repository
import com.lpirro.cscore.utils.AppSchedulerProvider
import com.lpirro.cscore.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(val application: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideRepository(): Repository {
        return Repository()
    }

    @Provides
    fun provideSchedulerProvider(): SchedulerProvider {
        return AppSchedulerProvider()
    }
}