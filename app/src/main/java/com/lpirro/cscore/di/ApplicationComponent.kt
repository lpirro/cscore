package com.lpirro.cscore.di

import com.lpirro.cscore.app.CSApplication
import com.lpirro.cscore.app.scorereport.ScoreReportFragment
import com.lpirro.cscore.app.scorereport.ScoreReportModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ScoreReportModule::class])
interface ApplicationComponent {
    fun inject(application: CSApplication)
    fun inject(fragment: ScoreReportFragment)
}