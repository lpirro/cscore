package com.lpirro.cscore.network.api

import com.lpirro.cscore.network.models.CreditReport
import io.reactivex.Single

open class Repository(var apiService: ApiService = ApiServiceFactory.makeApiService()) {

    fun getCreditReport(): Single<CreditReport> {
        return apiService.getCreditReport()
    }
}
