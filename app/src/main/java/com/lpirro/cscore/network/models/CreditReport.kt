package com.lpirro.cscore.network.models


data class CreditReport (val creditReportInfo: CreditReportInfo)