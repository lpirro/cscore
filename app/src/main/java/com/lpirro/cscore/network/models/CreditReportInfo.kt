package com.lpirro.cscore.network.models

data class CreditReportInfo(val score: Int, val maxScoreValue: Int, val minScorevalue: Int)