package com.lpirro.cscore.network.api

import com.lpirro.cscore.network.models.CreditReport
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {

    @GET("/prod/mockcredit/values")
    fun getCreditReport(): Single<CreditReport>

}