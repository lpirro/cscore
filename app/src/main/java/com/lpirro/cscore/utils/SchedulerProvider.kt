package com.lpirro.cscore.utils

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface SchedulerProvider {
    fun uiScheduler() : Scheduler
    fun ioScheduler() : Scheduler
}

class AppSchedulerProvider : SchedulerProvider {
    override fun ioScheduler() = Schedulers.io()
    override fun uiScheduler(): Scheduler = AndroidSchedulers.mainThread()
}

open class TestSchedulerProvider : SchedulerProvider {
    override fun uiScheduler() = Schedulers.trampoline()
    override fun ioScheduler() = Schedulers.trampoline()
}