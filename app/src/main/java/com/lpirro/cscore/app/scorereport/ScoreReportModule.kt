package com.lpirro.cscore.app.scorereport

import com.lpirro.cscore.network.api.Repository
import com.lpirro.cscore.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ScoreReportModule{

    @Provides
    @Singleton
    fun provideScoreReportPresenter (repository: Repository, schedulerProvider: SchedulerProvider): ScoreReportPresenter {
        return ScoreReportPresenter(repository, schedulerProvider)
    }
}