package com.lpirro.cscore.app.scorereport

import com.lpirro.cscore.mvp.BasePresenter
import com.lpirro.cscore.network.api.Repository
import com.lpirro.cscore.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

open class ScoreReportPresenter(private val repository: Repository, private val schedulerProvider: SchedulerProvider):
    BasePresenter<ScoreReportContract.ScoreReportView>(), ScoreReportContract.Presenter {

    private val mCompositeDisposable = CompositeDisposable()

    override fun unsubscribe() {
        mCompositeDisposable.clear()
    }

    override fun loadScoreReport() {

        view?.showLoading(true)

        val disposable =  repository.getCreditReport()
            .subscribeOn(schedulerProvider.ioScheduler())
            .observeOn(schedulerProvider.uiScheduler())
            .map { response -> response.creditReportInfo }
            .doFinally { view?.showLoading(false) }
            .subscribe(
                { scoreReport -> view?.showScoreReport(scoreReport) },
                { error  -> view?.showError(error) }
            )

        mCompositeDisposable.add(disposable)
    }
}