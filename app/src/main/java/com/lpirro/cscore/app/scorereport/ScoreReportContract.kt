package com.lpirro.cscore.app.scorereport

import com.lpirro.cscore.mvp.View
import com.lpirro.cscore.network.models.CreditReportInfo

interface ScoreReportContract {

    interface ScoreReportView: View {
        fun showScoreReport(reportInfo: CreditReportInfo)
    }

    interface Presenter {
        fun loadScoreReport()
    }
}