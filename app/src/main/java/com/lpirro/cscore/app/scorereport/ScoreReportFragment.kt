package com.lpirro.cscore.app.scorereport

import android.content.Context
import android.os.Bundle
import android.view.View
import com.lpirro.cscore.R
import com.lpirro.cscore.app.CSApplication
import com.lpirro.cscore.app.base.BaseFragment
import com.lpirro.cscore.network.models.CreditReportInfo
import kotlinx.android.synthetic.main.fragment_score_report.*
import javax.inject.Inject

class ScoreReportFragment: BaseFragment(), ScoreReportContract.ScoreReportView {

    override fun layoutId() = R.layout.fragment_score_report

    @Inject lateinit var presenter: ScoreReportPresenter
    private var reportInfo: CreditReportInfo? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (activity?.application as CSApplication).component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.attachView(this)

        if(savedInstanceState == null)
            presenter.loadScoreReport()

        swipeRefresh.setOnRefreshListener { presenter.loadScoreReport() }
    }

    override fun showScoreReport(reportInfo: CreditReportInfo) {
        updateScoreView(reportInfo)
    }

    override fun showLoading(active: Boolean) {
        swipeRefresh.isRefreshing = active
    }

    override fun showError(error: Throwable) {
        showAlertDialog(message = error.localizedMessage)
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    private fun updateScoreView(reportInfo: CreditReportInfo, animationEnabled: Boolean = true){
        this.reportInfo = reportInfo
        clearScoreView.maxValue = reportInfo.maxScoreValue.toFloat()
        clearScoreView.minValue = reportInfo.minScorevalue.toFloat()
        clearScoreView.setValue(reportInfo.score.toFloat(), animationEnabled)
    }
}