package com.lpirro.cscore.app

import android.app.Application
import com.lpirro.cscore.app.scorereport.ScoreReportModule
import com.lpirro.cscore.di.ApplicationComponent
import com.lpirro.cscore.di.ApplicationModule
import com.lpirro.cscore.di.DaggerApplicationComponent

class CSApplication: Application(){

    lateinit var component: ApplicationComponent
        private set

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .scoreReportModule(ScoreReportModule())
            .build()
    }
}