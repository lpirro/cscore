package com.lpirro.cscore.app.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.lpirro.cscore.R

abstract class BaseFragment : Fragment() {

    @LayoutRes
    protected abstract fun layoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId(), container, false)
    }

    fun showAlertDialog(title: String = getString(R.string.app_name), message: String) {

        AlertDialog.Builder(context!!).apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton(android.R.string.ok) { dialog, which ->
                dialog.dismiss()
            }
        }.show()
    }

}